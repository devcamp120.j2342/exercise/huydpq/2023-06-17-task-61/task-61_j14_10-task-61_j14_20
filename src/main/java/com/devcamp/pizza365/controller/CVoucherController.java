package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.repository.IVoucherRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
            // voucherRepository.findAll().forEach((ele) -> {
            // listVoucher.add(ele);
            // });
            voucherRepository.findAll().forEach(listVoucher::add);
            if (listVoucher.size() == 0) {
                return new ResponseEntity<List<CVoucher>>(listVoucher, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CVoucher>>(listVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
