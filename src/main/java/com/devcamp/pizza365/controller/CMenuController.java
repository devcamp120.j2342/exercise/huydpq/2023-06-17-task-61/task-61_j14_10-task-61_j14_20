package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IMenuRepository;


@RestController
@RequestMapping("/")
@CrossOrigin
public class CMenuController {
    @Autowired
    IMenuRepository menuRepository;

    @GetMapping("/combo-menus")
    public ResponseEntity<List<CMenu>> getMenu() {
        try {
            List<CMenu> listMenu = new ArrayList<CMenu>();
            menuRepository.findAll().forEach(listMenu::add);
            if (listMenu.size() == 0) {
                return new ResponseEntity<List<CMenu>>(listMenu, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CMenu>>(listMenu, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
