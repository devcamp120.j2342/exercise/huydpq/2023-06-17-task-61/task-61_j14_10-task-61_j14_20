package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.repository.IOrderRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class COrderController {
    @Autowired
    IOrderRepository pOrderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllEmployees() {
        try {
            List<COrder> listOrder = new ArrayList<COrder>();
            pOrderRepository.findAll().forEach(listOrder::add);
            return new ResponseEntity<>(listOrder, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
